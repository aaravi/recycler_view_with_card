package com.example.ravis.recycler_view_with_card;

import java.util.ArrayList;

/**
 * Created by ravis on 6/7/2017.
 */

public class DummyValues {
    ArrayList<DummyData> list=new ArrayList<>();

    DummyValues() {
       list = new ArrayList<>();
        DummyData obj = new DummyData(R.drawable.img1,  "Title1", "Description1", "#C0392B");
        list.add(obj);
        DummyData obj1 = new DummyData(R.drawable.img2, "Title2", "Description2", "#A569BD");
        list.add(obj1);
        DummyData obj2 = new DummyData(R.drawable.img3, "Title3", "Description3", "#3498DB");
        list.add(obj2);
        DummyData obj3 = new DummyData(R.drawable.img4, "Title4", "Description4", "#1ABC9C");
        list.add(obj3);
        DummyData obj4 = new DummyData(R.drawable.img5, "Title5", "Description5", "#A6ACAF");
        list.add(obj4);
        DummyData obj5 = new DummyData(R.drawable.img6, "Title6", "Description6", "#7D3C98");
        list.add(obj5);
        DummyData obj6 = new DummyData(R.drawable.img2, "Title7", "Description7", "#196F3D");
        list.add(obj6);
        DummyData obj7 = new DummyData(R.drawable.img1, "Title8", "Description8", "#F4D03F");
        list.add(obj7);
        DummyData obj8 = new DummyData(R.drawable.img3, "Title9", "Description9", "#D0D3D4");
        list.add(obj8);
        DummyData obj9 = new DummyData(R.drawable.img6, "Title10", "Description10", "#F0D3D4");
        list.add(obj9);

    }
}
