package com.example.ravis.recycler_view_with_card;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

/**
 * Created by ravis on 6/7/2017.
 */

public class cardAdapter extends RecyclerView.Adapter<cardAdapter.viewHolder> {
    DummyValues values;
    Context context;
    public cardAdapter(DummyValues values,Context context){
        this.context=context;
this.values=values;
}
    public static class viewHolder extends RecyclerView.ViewHolder{
        public CardView cardView;
        public ImageView img;
        public TextView title;
        public TextView desc;
        public String color;
        public viewHolder(View itemView) {
            super(itemView);
            cardView=(CardView) itemView.findViewById(R.id.maincardview);
            img=(ImageView)itemView.findViewById(R.id.cardviewimage);
            title=(TextView)itemView.findViewById(R.id.CardTitle);
            desc=(TextView)itemView.findViewById(R.id.CardDescription);
            color="";
        }
    }
    @Override
    public cardAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext()).inflate(R.layout.mycardview, parent, false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, final int position) {

holder.desc.setText(values.list.get(position).getDesc());
        holder.title.setText(values.list.get(position).getTitle());
        holder.cardView.setBackgroundColor(Color.parseColor(values.list.get(position).getColor()));
        Picasso.with(context)
                .load(values.list.get(position).getImg())
                .placeholder(R.drawable.webs)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.img);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context,"This is "+values.list.get(position).title,Toast.LENGTH_SHORT).show();
                Intent i=new Intent(context,CreateRule.class);
                i.putExtra("Title",values.list.get(position).getTitle());
                i.putExtra("desc",values.list.get(position).getDesc());
                i.putExtra("images",values.list.get(position).getImg()+"");
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.list.size();
    }
}
