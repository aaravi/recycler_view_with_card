package com.example.ravis.recycler_view_with_card;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class CreateRule extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_rule);
        Toolbar toolbar=(Toolbar)findViewById(R.id.tool);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        TextView toolbartext=(TextView)findViewById(R.id.text);
        String text = getIntent().getStringExtra("Title");
        toolbartext.setText(text);
        TextView descriptiontext=(TextView)findViewById(R.id.desc);
        String text1 = getIntent().getStringExtra("desc");
        descriptiontext.setText("Create"+text1);
        ImageView imagedesc=(ImageView)findViewById(R.id.cardviewimage);
        String image = getIntent().getStringExtra("images");
        //Toast.makeText(this,"This is "+image,Toast.LENGTH_SHORT).show();
        Picasso.with(this.getApplicationContext())
                .load(image)
                .placeholder(R.drawable.webs)
                .error(android.R.drawable.stat_notify_error)
                .into(imagedesc);



    }
}

